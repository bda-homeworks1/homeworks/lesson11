package HomeWorks.lesson11.tasks.services.ServicesClasses;


import HomeWorks.lesson11.tasks.classes.Employee;
import HomeWorks.lesson11.tasks.services.InterfaceServices.EmployeeService;

import java.util.ArrayList;
import java.util.Scanner;

public class EmployeeServiceImpl implements EmployeeService {
    ArrayList<Employee> employees = new ArrayList<>();
    Scanner scanner = new Scanner(System.in);
    int id = 1;
    int t = -1;
    @Override
    public String toString() {
        return "EmployeeServiceImpl{" +
                "employees=" + employees +
                '}';
    }

    @Override
    public void register() {

        System.out.println("register ucun - 1 ,  exit ucun - 2, show - 3 , update - 4, delete -5, findByName - 6");
        String command = scanner.next();
        switch (command){
            case "1":
                System.out.println("username:");
                String username = scanner.next();
                System.out.println("password:");
                String password = scanner.next();
                System.out.println("salary(only number):");
                int salary = scanner.nextInt();
                Employee empl = new Employee(id,username,password,salary);
                id+=1;
                employees.add(empl);
                register();
                break;
            case "2":
                System.out.println("cixis verildi");
                break;
            case "3":
                show();
                break;
            case "4":
                update();
                break;
            case "5":
                delete();
                break;
            case "6":
                findByName();
                break;
        }
    }

    @Override
    public void show() {
       if(employees.size()>0){
           for (int i = 0; i <employees.size() ; i++) {
//               System.out.println("id:" + employees.get(i).getId());
//               System.out.println("username:" + employees.get(i).getUsername());
//               System.out.println("password:" + employees.get(i).getPassword());
               System.out.println(employees.get(i).toString());
           }
       }else{
           System.out.println("isci yoxdur");
       }
    }

    @Override
    public void update() {
        System.out.println("iscinin id-sini secin");
        int idCommand = scanner.nextInt();

        for (int i = 0; i <employees.size() ; i++) {
            if(employees.get(i).getId()==idCommand){
                t=i;
            }
        }

        if(t<0){
            System.out.println("axtardiginiz id-de isci tapilmadi");
            t=-1;
            update();
        }else{
            System.out.println("iscinin deyismek istediyiniz melumati:");
            System.out.println("username - 1");
            System.out.println("password - 2");
            System.out.println("salary - 3");
            String com = scanner.next();
            switch (com){
                case "1":
                    changeUsername();
                    break;
                case "2":
                    changePassword();
                    break;
                case "3":
                    changeSalary();
                    break;
            }

        }

    }

    private void changeUsername(){
        System.out.println("yeni username - i daxil edin");
        String newUsername = scanner.next();
        employees.get(t).setUsername(newUsername);
        System.out.println("ad ugurla deyisdirildi");
        show();
    }

    private void changePassword(){

        System.out.println("yeni parol daxil edin");
        String newPassword = scanner.next();
        System.out.println("yeni parolu bir daha tekrar edin:");
        String newPassword2 = scanner.next();
        if (newPassword.equals(newPassword2)){
            employees.get(t).setPassword(newPassword);
            System.out.println("yeni password ugurla deyisdirildi");
            show();

        }else {
            System.out.println("yeni parolu duzgun tekrar yazin:");
            changePassword();


        }
    }

    private void changeSalary(){
        System.out.println("yeni maas (number) - i daxil edin");
        int newSalary = scanner.nextInt();
        employees.get(t).setSalary(newSalary);
        System.out.println("maas ugurla deyisdirildi");
        show();
    }

    @Override
    public void delete() {
        System.out.println("silmek istediyiniz id-ni yazin");
        int removeId = scanner.nextInt();
        for (int i = 0; i < employees.size() ; i++) {
            if(employees.get(i).getId()==removeId){
                t=i;
            }
        }

        if(t<0){
            System.out.println("silmek istediyiniz id-de isci tapilmadi");
            t=-1;
            delete();
        }else{
            employees.remove(t);
            System.out.println("ugurla silindi");
            show();
        }
    }

    @Override
    public void findByName() {
        System.out.println("axtardiginiz username-i yazin");
        String findName = scanner.next();
        for (int i = 0; i <employees.size() ; i++) {
            if(employees.get(i).getUsername().toLowerCase().contains(findName.toLowerCase())){
                t=i;
            }
        }

        if(t<0){
            System.out.println("yazdiginiz username tapilmadi");
            findByName();
        }else{
            System.out.println(employees.get(t));
        }
    }
}
