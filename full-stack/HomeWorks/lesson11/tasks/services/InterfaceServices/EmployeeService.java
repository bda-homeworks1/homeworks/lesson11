package HomeWorks.lesson11.tasks.services.InterfaceServices;

import HomeWorks.lesson11.tasks.classes.Employee;

import java.util.ArrayList;

public interface EmployeeService {
     void register();
     void show();
     void update();
     void delete();
     void findByName();
}
